package wow

import (
	"os"

	"github.com/mailru/easyjson"
	"github.com/valyala/fasthttp"

	"gitlab.com/pgarin/bnet/internal"
)

//go:generate easyjson
//easyjson:json
type Profile struct {
	WoWAccounts []struct {
		ID         int         `json:"id"`
		Characters []Character `json:"characters"`
	} `json:"wow_accounts"`
}

func (p *Profile) Reset() {
	if len(p.WoWAccounts) > 0 {
		if len(p.WoWAccounts) > 1 {
			// Removing excessive references so that GC could collect related objects.
			for i := 1; i < len(p.WoWAccounts); i++ {
				p.WoWAccounts[i].Characters = nil
			}
		}
		// It's quire rare to have more than one WoW account since WoW Classic is separated.
		p.WoWAccounts = p.WoWAccounts[:1]
		p.WoWAccounts[0].ID = 0

		chars := p.WoWAccounts[0].Characters
		// Even though per-account maximum is 50, it's very uncommon to have so many characters.
		if len(chars) > 8 {
			chars = make([]Character, 0, 8)
		} else {
			p.WoWAccounts[0].Characters = chars[:0]
		}
	}
}

func (app *App) GetProfile(p *Profile, region, locale, token string) error {
	ctx := internal.AcquireReqCtx()
	app.WriteApiHost(ctx.Buf, region)
	_, _ = ctx.Buf.WriteString("/profile/user/wow")
	_, _ = ctx.Buf.WriteString("?namespace=profile-")
	_, _ = ctx.Buf.WriteString(region)
	_, _ = ctx.Buf.WriteString("&locale=")
	_, _ = ctx.Buf.WriteString(locale)
	_, _ = ctx.Buf.WriteString("&access_token=")
	_, _ = ctx.Buf.WriteString(token)
	ctx.Req.SetRequestURIBytes(ctx.Buf.Bytes())
	ctx.Req.Header.SetMethod(fasthttp.MethodGet)

	if err := internal.HTTPClient.Do(&ctx.Req, &ctx.Res); err != nil {
		return err
	}
	if ctx.Res.StatusCode() != fasthttp.StatusOK {
		return os.ErrInvalid
	}

	err := easyjson.Unmarshal(ctx.Res.Body(), p)
	internal.ReleaseReqCtx(ctx)
	return err
}

func (app *App) GetProfileBytes(p *Profile, region, locale, token []byte) error {
	return app.GetProfile(p, internal.B2S(region), internal.B2S(locale), internal.B2S(token))
}
