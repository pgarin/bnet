// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package wow

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjson96521307DecodeGitlabComPgarinBnetWow(in *jlexer.Lexer, out *Character) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "id":
			out.ID = int(in.Int())
		case "name":
			out.Name = string(in.String())
		case "realm":
			easyjson96521307Decode(in, &out.Realm)
		case "playable_class":
			easyjson96521307Decode1(in, &out.Class)
		case "playable_race":
			easyjson96521307Decode1(in, &out.Race)
		case "gender":
			easyjson96521307Decode2(in, &out.Gender)
		case "faction":
			easyjson96521307Decode2(in, &out.Faction)
		case "level":
			out.Level = int(in.Int())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson96521307EncodeGitlabComPgarinBnetWow(out *jwriter.Writer, in Character) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"id\":"
		out.RawString(prefix[1:])
		out.Int(int(in.ID))
	}
	{
		const prefix string = ",\"name\":"
		out.RawString(prefix)
		out.String(string(in.Name))
	}
	{
		const prefix string = ",\"realm\":"
		out.RawString(prefix)
		easyjson96521307Encode(out, in.Realm)
	}
	{
		const prefix string = ",\"playable_class\":"
		out.RawString(prefix)
		easyjson96521307Encode1(out, in.Class)
	}
	{
		const prefix string = ",\"playable_race\":"
		out.RawString(prefix)
		easyjson96521307Encode1(out, in.Race)
	}
	{
		const prefix string = ",\"gender\":"
		out.RawString(prefix)
		easyjson96521307Encode2(out, in.Gender)
	}
	{
		const prefix string = ",\"faction\":"
		out.RawString(prefix)
		easyjson96521307Encode2(out, in.Faction)
	}
	{
		const prefix string = ",\"level\":"
		out.RawString(prefix)
		out.Int(int(in.Level))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Character) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson96521307EncodeGitlabComPgarinBnetWow(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Character) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson96521307EncodeGitlabComPgarinBnetWow(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Character) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson96521307DecodeGitlabComPgarinBnetWow(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Character) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson96521307DecodeGitlabComPgarinBnetWow(l, v)
}
func easyjson96521307Decode2(in *jlexer.Lexer, out *struct {
	Type string `json:"type,intern"`
	Name string `json:"name,intern"`
}) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "type":
			out.Type = string(in.StringIntern())
		case "name":
			out.Name = string(in.StringIntern())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson96521307Encode2(out *jwriter.Writer, in struct {
	Type string `json:"type,intern"`
	Name string `json:"name,intern"`
}) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"type\":"
		out.RawString(prefix[1:])
		out.String(string(in.Type))
	}
	{
		const prefix string = ",\"name\":"
		out.RawString(prefix)
		out.String(string(in.Name))
	}
	out.RawByte('}')
}
func easyjson96521307Decode1(in *jlexer.Lexer, out *struct {
	ID   int    `json:"id"`
	Name string `json:"name,intern"`
}) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "id":
			out.ID = int(in.Int())
		case "name":
			out.Name = string(in.StringIntern())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson96521307Encode1(out *jwriter.Writer, in struct {
	ID   int    `json:"id"`
	Name string `json:"name,intern"`
}) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"id\":"
		out.RawString(prefix[1:])
		out.Int(int(in.ID))
	}
	{
		const prefix string = ",\"name\":"
		out.RawString(prefix)
		out.String(string(in.Name))
	}
	out.RawByte('}')
}
func easyjson96521307Decode(in *jlexer.Lexer, out *struct {
	ID   int    `json:"id"`
	Name string `json:"name,intern"`
	Slug string `json:"slug,intern"`
}) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "id":
			out.ID = int(in.Int())
		case "name":
			out.Name = string(in.StringIntern())
		case "slug":
			out.Slug = string(in.StringIntern())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson96521307Encode(out *jwriter.Writer, in struct {
	ID   int    `json:"id"`
	Name string `json:"name,intern"`
	Slug string `json:"slug,intern"`
}) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"id\":"
		out.RawString(prefix[1:])
		out.Int(int(in.ID))
	}
	{
		const prefix string = ",\"name\":"
		out.RawString(prefix)
		out.String(string(in.Name))
	}
	{
		const prefix string = ",\"slug\":"
		out.RawString(prefix)
		out.String(string(in.Slug))
	}
	out.RawByte('}')
}
