package wow

//go:generate easyjson
//easyjson:json
type Character struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Realm struct {
		ID   int    `json:"id"`
		Name string `json:"name,intern"`
		Slug string `json:"slug,intern"`
	} `json:"realm"`
	Class struct {
		ID   int    `json:"id"`
		Name string `json:"name,intern"`
	} `json:"playable_class"`
	Race struct {
		ID   int    `json:"id"`
		Name string `json:"name,intern"`
	} `json:"playable_race"`
	Gender struct {
		Type string `json:"type,intern"`
		Name string `json:"name,intern"`
	} `json:"gender"`
	Faction struct {
		Type string `json:"type,intern"`
		Name string `json:"name,intern"`
	} `json:"faction"`
	Level int `json:"level"`
}

func (ch *Character) Reset() {
	ch.ID = 0
	ch.Name = ""
	ch.Realm.ID = 0
	ch.Realm.Name = ""
	ch.Class.ID = 0
	ch.Class.Name = ""
	ch.Race.ID = 0
	ch.Race.Name = ""
	ch.Gender.Type = ""
	ch.Gender.Name = ""
	ch.Faction.Type = ""
	ch.Faction.Name = ""
	ch.Level = 0
}
