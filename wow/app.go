package wow

import (
	"gitlab.com/pgarin/bnet/internal"
)

type App struct{}

func NewApp() *App {
	return &App{}
}

func (app *App) WriteApiHost(w internal.XWriter, region string) {
	_, _ = w.WriteString("https://")
	if region == RegionCN {
		_, _ = w.WriteString("gateway.battlenet.com.cn")
	} else {
		_, _ = w.WriteString(region)
		_, _ = w.WriteString(".api.blizzard.com")
	}
}
