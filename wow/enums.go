package wow

import (
	"gitlab.com/pgarin/bnet/internal"
)

const (
	RegionUS = "us"
	RegionEU = "eu"
	RegionKR = "kr"
	RegionTW = "tw"
	RegionCN = "cn"
)

func CheckRegion(v string) bool {
	switch v {
	case RegionUS:
		return true
	case RegionEU:
		return true
	case RegionKR:
		return true
	case RegionTW:
		return true
	case RegionCN:
		return true
	default:
		return false
	}
}

func CheckRegionBytes(b []byte) bool {
	return CheckRegion(internal.B2S(b))
}

const (
	LocaleEnUs = "en_US"
	LocaleEsMx = "es_MX"
	LocalePt   = "pt_BR"
	LocaleDe   = "de_DE"
	LocaleEnGb = "en_GB"
	LocaleEsEs = "es_ES"
	LocaleFr   = "fr_FR"
	LocaleIt   = "it_IT"
	LocaleRu   = "ru_RU"
	LocaleKo   = "ko_KR"
	LocaleZhTw = "zh_TW"
	LocaleZhCn = "zh_CN"
)

func CheckLocale(v string) bool {
	switch v {
	case LocaleEnUs:
		return true
	case LocaleEsMx:
		return true
	case LocalePt:
		return true
	case LocaleDe:
		return true
	case LocaleEnGb:
		return true
	case LocaleEsEs:
		return true
	case LocaleFr:
		return true
	case LocaleIt:
		return true
	case LocaleRu:
		return true
	case LocaleKo:
		return true
	case LocaleZhTw:
		return true
	case LocaleZhCn:
		return true
	default:
		return false
	}
}

func CheckLocaleBytes(b []byte) bool {
	return CheckLocale(internal.B2S(b))
}
