package bnet

import (
	"bytes"
	"encoding/base64"

	"gitlab.com/pgarin/bnet/internal"
	"gitlab.com/pgarin/bnet/wow"
)

type App struct {
	ClientId     string
	ClientSecret string
	WoW          *wow.App

	basicAuth string
}

func NewApp(clientId, clientSecret string) *App {
	app := &App{}
	app.ClientId = clientId
	app.ClientSecret = clientSecret
	app.WoW = wow.NewApp()

	b := bytes.Buffer{}
	b.WriteString(clientId)
	b.WriteByte(':')
	b.WriteString(clientSecret)
	app.basicAuth = "Basic " + base64.StdEncoding.EncodeToString(b.Bytes())
	return app
}

func (app *App) WriteApiHost(w internal.XWriter, region string) {
	_, _ = w.WriteString("https://")
	if region == RegionCN {
		_, _ = w.WriteString("www.battlenet.com.cn")
	} else {
		_, _ = w.WriteString(region)
		_, _ = w.WriteString(".battle.net")
	}
}
