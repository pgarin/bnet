package tests

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/pgarin/bnet"
	"gitlab.com/pgarin/bnet/internal"
)

func TestBakeOAuthUrl(t *testing.T) {
	type TC struct {
		Name        string
		Region      string
		Scopes      bnet.OAuthScope
		RedirectUri string
		Expectancy  string
	}
	tcs := []TC{
		{"eu", bnet.RegionEU, bnet.OAuthWow, "https://example.com",
			"https://eu.battle.net/oauth/authorize?response_type=code&scope=wow.profile&client_id=foo&redirect_uri=https%3A%2F%2Fexample.com"},
		{"cn", bnet.RegionCN, bnet.OAuthWow, "bar",
			"https://www.battlenet.com.cn/oauth/authorize?response_type=code&scope=wow.profile&client_id=foo&redirect_uri=bar"},
		{"multiple scopes", bnet.RegionUS, bnet.OAuthWow | bnet.OAuthD3, "bar",
			"https://us.battle.net/oauth/authorize?response_type=code&scope=wow.profile%20d3.profile&client_id=foo&redirect_uri=bar"},
		{"malformed url", "", 1 << 7, "",
			"https://.battle.net/oauth/authorize?response_type=code&scope=&client_id=foo&redirect_uri="},
	}

	app := bnet.NewApp("foo", "bar")
	for _, tc := range tcs {
		t.Run(tc.Name, func(t *testing.T) {
			w := &bytes.Buffer{}
			redirectUri := app.BakeOAuthUrl(w, tc.Region, tc.Scopes)
			_, _ = redirectUri.WriteString(tc.RedirectUri)
			assert.Equal(t, tc.Expectancy, w.String())
		})
	}
}

func BenchmarkBakeOAuthUrl(b *testing.B) {
	type TC struct {
		Name        string
		Region      string
		Scopes      bnet.OAuthScope
		RedirectUri string
		Expectancy  string
	}
	tcs := []TC{
		{"eu", bnet.RegionEU, bnet.OAuthWow, "https://example.com",
			"https://eu.battle.net/oauth/authorize?response_type=code&scope=wow.profile&client_id=foo&redirect_uri=https%3A%2F%2Fexample.com"},
		{"cn", bnet.RegionCN, bnet.OAuthWow, "bar",
			"https://www.battlenet.com.cn/oauth/authorize?response_type=code&scope=wow.profile&client_id=foo&redirect_uri=bar"},
		{"multiple scopes", bnet.RegionUS, bnet.OAuthWow | bnet.OAuthD3, "bar",
			"https://us.battle.net/oauth/authorize?response_type=code&scope=wow.profile%20d3.profile&client_id=foo&redirect_uri=bar"},
		{"malformed url", "", 1 << 7, "",
			"https://.battle.net/oauth/authorize?response_type=code&scope=&client_id=foo&redirect_uri="},
	}

	app := bnet.NewApp("foo", "bar")
	for _, tc := range tcs {
		b.Run(tc.Name, func(b *testing.B) {
			w := bytes.NewBuffer(make([]byte, 0, 512))
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				redirectUri := app.BakeOAuthUrl(w, tc.Region, tc.Scopes)
				_, _ = redirectUri.WriteString(tc.RedirectUri)

				if !bytes.Equal(w.Bytes(), internal.S2B(tc.Expectancy)) {
					b.FailNow()
				}
				w.Reset()
			}
			b.ReportAllocs()
		})
	}
}
