module gitlab.com/pgarin/bnet

go 1.16

require (
	github.com/mailru/easyjson v0.7.7
	github.com/stretchr/testify v1.7.0
	github.com/valyala/fasthttp v1.28.0
	github.com/valyala/fastjson v1.6.3
)
