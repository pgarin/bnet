package bnet

import (
	"os"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"

	"gitlab.com/pgarin/bnet/internal"
)

func (app *App) BakeOAuthUrl(w internal.XWriter, region string, scopes OAuthScope) internal.UrlEncodeXWriter {
	app.WriteApiHost(w, region)
	_, _ = w.WriteString("/oauth/authorize?response_type=code&scope=")
	tryAddScope := func(key OAuthScope) {
		if scopes&key == key {
			_, _ = w.WriteString(key.toString())
			scopes ^= key

			if scopes > 0 {
				const space = "%20"
				_, _ = w.WriteString(space)
			}
		}
	}
	tryAddScope(OAuthWow)
	tryAddScope(OAuthSc2)
	tryAddScope(OAuthD3)
	tryAddScope(OAuthOpenId)

	_, _ = w.WriteString("&client_id=")
	_, _ = w.WriteString(app.ClientId)
	_, _ = w.WriteString("&redirect_uri=")
	return internal.UrlEncodeXWriter{XWriter: w}
}

func (app *App) BakeOAuthUrlBytes(w internal.XWriter, region []byte, scopes OAuthScope) internal.UrlEncodeXWriter {
	return app.BakeOAuthUrl(w, internal.B2S(region), scopes)
}

func (app *App) GetOAuthToken(region, code string, writeRedirectUri func(internal.UrlEncodeXWriter)) (string, error) {
	ctx := internal.AcquireReqCtx()

	app.WriteApiHost(ctx.Buf, region)
	_, _ = ctx.Buf.WriteString("/oauth/token")
	ctx.Req.SetRequestURIBytes(ctx.Buf.Bytes())
	ctx.Req.Header.SetMethod(fasthttp.MethodPost)
	ctx.Req.Header.SetContentType("application/x-www-form-urlencoded")
	ctx.Req.Header.Set(fasthttp.HeaderAuthorization, app.basicAuth)

	ctx.Buf.Reset()
	ctx.Buf.WriteString("grant_type=authorization_code&code=")
	_, _ = ctx.Buf.WriteString(code)
	_, _ = ctx.Buf.WriteString("&redirect_uri=")
	writeRedirectUri(internal.UrlEncodeXWriter{XWriter: ctx.Buf})
	ctx.Req.SetBody(ctx.Buf.Bytes())

	if err := internal.HTTPClient.Do(&ctx.Req, &ctx.Res); err != nil {
		return "", err
	}
	if ctx.Res.StatusCode() != fasthttp.StatusOK {
		return "", os.ErrInvalid
	}

	fj, err := fastjson.ParseBytes(ctx.Res.Body())
	if err != nil {
		return "", err
	}

	token := fj.Get("access_token")
	if token == nil {
		return "", os.ErrInvalid
	}

	internal.ReleaseReqCtx(ctx)
	return token.String(), nil
}

func (app *App) GetOAuthTokenBytes(region, code []byte, writeRedirectUri func(internal.UrlEncodeXWriter)) (string, error) {
	return app.GetOAuthToken(internal.B2S(region), internal.B2S(code), writeRedirectUri)
}
