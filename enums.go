package bnet

import (
	"gitlab.com/pgarin/bnet/internal"
)

const (
	RegionUS   = "us"
	RegionEU   = "eu"
	RegionAPAC = "apac"
	RegionCN   = "cn"
)

func CheckRegion(v string) bool {
	switch v {
	case RegionUS:
		return true
	case RegionEU:
		return true
	case RegionAPAC:
		return true
	case RegionCN:
		return true
	default:
		return false
	}
}

func CheckRegionBytes(b []byte) bool {
	return CheckRegion(internal.B2S(b))
}

type OAuthScope uint8

func (s OAuthScope) toString() string {
	switch s {
	case OAuthWow:
		return "wow.profile"
	case OAuthSc2:
		return "sc2.profile"
	case OAuthD3:
		return "d3.profile"
	case OAuthOpenId:
		return "openid"
	default:
		return "undefined"
	}
}

const (
	OAuthWow    OAuthScope = 1
	OAuthSc2    OAuthScope = 1 << 1
	OAuthD3     OAuthScope = 1 << 2
	OAuthOpenId OAuthScope = 1 << 3
)
