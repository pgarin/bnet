package internal

import (
	"bytes"
	"sync"

	"github.com/valyala/fasthttp"
)

type ReqCtx struct {
	Req fasthttp.Request
	Res fasthttp.Response
	Buf *bytes.Buffer
}

func (c *ReqCtx) Reset() {
	c.Req.Reset()
	c.Res.Reset()
	c.Buf.Reset()
}

var (
	HTTPClient = &fasthttp.Client{}
	reqCtxPool = sync.Pool{
		New: func() interface{} {
			return ReqCtx{
				Req: fasthttp.Request{},
				Res: fasthttp.Response{},
				Buf: bytes.NewBuffer(make([]byte, 0, 512)),
			}
		},
	}
)

func AcquireReqCtx() *ReqCtx {
	return reqCtxPool.Get().(*ReqCtx)
}

func ReleaseReqCtx(c *ReqCtx) {
	c.Reset()
	reqCtxPool.Put(c)
}
