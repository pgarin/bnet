package internal

import (
	"io"
)

type XWriter interface {
	io.Writer
	io.ByteWriter
	io.StringWriter
}

type UrlEncodeXWriter struct {
	XWriter
}

func (w *UrlEncodeXWriter) Write(b []byte) (int, error) {
	return w.WriteString(B2S(b))
}

func (w *UrlEncodeXWriter) WriteByte(b byte) error {
	hexCharUpper := func(c byte) byte {
		if c < 10 {
			return '0' + c
		}
		return c - 10 + 'A'
	}
	// See http://www.w3.org/TR/html5/forms.html#form-submission-algorithm
	if b >= 'a' && b <= 'z' || b >= 'A' && b <= 'Z' || b >= '0' && b <= '9' ||
		b == '-' || b == '.' || b == '_' {
		_ = w.XWriter.WriteByte(b)
	} else {
		if b == ' ' {
			_ = w.XWriter.WriteByte('+')
		} else {
			_ = w.XWriter.WriteByte('%')
			_ = w.XWriter.WriteByte(hexCharUpper(b >> 4))
			_ = w.XWriter.WriteByte(hexCharUpper(b & 15))
		}
	}
	return nil
}

func (w *UrlEncodeXWriter) WriteString(s string) (int, error) {
	if len(s) == 0 {
		return 0, nil
	}
	n := len(s)
	_ = s[n-1]

	i := 0
loop:
	w.WriteByte(s[i])
	i++
	if i < n {
		goto loop
	}
	return 0, nil
}
